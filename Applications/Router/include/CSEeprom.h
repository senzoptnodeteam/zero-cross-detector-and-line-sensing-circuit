/*
 * CSEeprom.h
 *
 * Created: 08-04-2015 10:06:57
 *  Author: Prateek
 */ 

#ifndef CS_EEPROM_H
#define CS_EEPROM_H

#include <WSNDemoApp.h>

typedef struct
{
  /** \brief EEPROM address */
  uint16_t address;
  /** \brief pointer to data memory */
  uint8_t *data;
  /** \brief number of bytes */
  uint16_t length;
} CS_EepromParams_t;

#define EEPROM_STORE_ADDRESS   0x00

int CS_ReadEeprom(CS_EepromParams_t *params);
int CS_WriteEeprom(CS_EepromParams_t *params);

#endif /* CS_EEPROM_H */
