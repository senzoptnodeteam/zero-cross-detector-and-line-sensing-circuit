/**************************************************************************//**
	\file	DeviceConfig.c

	\brief Implementation of DeVice Configuration.

	\author
		Senzopt \n

	\internal
	History:
		24/01/16 Senzopt
 ******************************************************************************/
/*****************************************************************************
                              Includes section
******************************************************************************/
#include <SZ_DbgLib.h>
#include <CSEeprom.h>
#include <DeviceConfig.h>
#include <WSNCommand.h>
#include <WSNDemoApp.h>
#include <config.h>
#include <resetReason.h>

static CS_EepromParams_t eepromParam;
DeviceConfig_t deviceConfig;

void read_device_config(void)
{
    uint8_t  i = 0;
	uint8_t *ptr;

	eepromParam.address = EEPROM_STORE_ADDRESS;			// offset for storing/reading cs config.
	eepromParam.length = sizeof(deviceConfig);
	eepromParam.data = (uint8_t *)&deviceConfig;
	CS_ReadEeprom(&eepromParam);

	if( deviceConfig.csParamUpdated != 0x01 )
	{
		DBG_Print("Device Not Configured\r\n");
		deviceConfig.deviceChannelMask = CHANNEL_MASK;
		deviceConfig.deviceMacId = DEVICE_ID;
		deviceConfig.devicePanId = EXT_PANID;
	}

	DBG_Print("configuraion_parameter_updated : %d\r\n",deviceConfig.csParamUpdated);
    DBG_Print("MacId : ");
    for(i = sizeof(deviceConfig.deviceMacId) - 1, ptr = (uint8_t *)&deviceConfig.deviceMacId; i; i--)
        DBG_Print("%02X",ptr[i]);
    DBG_Print("%02X\r\n",ptr[0]);
    DBG_Print("ExtendedPanId : ");
    for(i = sizeof(deviceConfig.devicePanId) - 1, ptr = (uint8_t *)&deviceConfig.devicePanId; i; i--)
        DBG_Print("%02X",ptr[i]);
    DBG_Print("%02X\n",ptr[0]);
    DBG_Print("ChannelMask : %02X\r\n",deviceConfig.deviceChannelMask);
    DBG_Print("_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ \r\n");

}


void changeDeviceConfig(AppCommand_t *pCommand)
{
	uint8_t  i = 0;
	uint8_t *ptr;
	deviceConfig.csParamUpdated = 0x01;
	deviceConfig.deviceChannelMask = pCommand->payload.deviceCMD.Config.deviceChannelMask;
	deviceConfig.deviceMacId = pCommand->payload.deviceCMD.Config.deviceMacId;	
	deviceConfig.devicePanId = pCommand->payload.deviceCMD.Config.devicePanId;

	DBG_Print("changeDeviceConfig updated\n");
  DBG_Print("MacId : ");
  for(i = sizeof(deviceConfig.deviceMacId) - 1, ptr = (uint8_t *)&deviceConfig.deviceMacId; i; i--)
    DBG_Print("%02X",ptr[i]);
  DBG_Print("%02X\r\n",ptr[0]);
  DBG_Print("ExtendedPanId : ");
  for(i = sizeof(deviceConfig.devicePanId) - 1, ptr = (uint8_t *)&deviceConfig.devicePanId; i; i--)
    DBG_Print("%02X",ptr[i]);
  DBG_Print("%02X\n",ptr[0]);
  DBG_Print("ChannelMask : %02X\r\n",deviceConfig.deviceChannelMask);

	eepromParam.address = EEPROM_STORE_ADDRESS;			// offset for storing/reading cs config.
	eepromParam.length = sizeof(deviceConfig);
	eepromParam.data = (uint8_t *)&deviceConfig;
	CS_WriteEeprom(&eepromParam);
	halDelayUs(500);

	/* reset the device to start fresh */
	HAL_WarmReset();
}
