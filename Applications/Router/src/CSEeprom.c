/*
 * CSEeprom.c
 *
 * Created: 08-04-2015 10:02:25
 *	Author: Prateek
 */ 

#include <config.h>
#include <CSEeprom.h>
#include <SZ_DbgLib.h>

#define CMD_KEY								0xA5
#define PAGE_SIZE							64
#define NO_OF_PAGES_PER_ROW					4
#define ROW_SIZE							(NO_OF_PAGES_PER_ROW * PAGE_SIZE)
#define ROW_SIZE_IN_HALF_WORDS				(ROW_SIZE / 2)
#define FLASH_TOTAL_PAGES					(0x40000ul / ROW_SIZE)

#define EEPROM_DATA_MEMORY_SIZE				0x1000u
#define FLASH_EEPROM_PAGES					(EEPROM_DATA_MEMORY_SIZE / ROW_SIZE)			// 16
#define FLASH_EEPROM_BASE_PAGE				(FLASH_TOTAL_PAGES - FLASH_EEPROM_PAGES)		// 1008
#define FLASH_EEPROM_BASE_ADDRESS			(FLASH_EEPROM_BASE_PAGE * ROW_SIZE)			// 258048

#define FLASH_READY()		(NVMCTRL_INTFLAG_s.ready)
#define FLASH_ADDR			(0x00000000U) /**< FLASH base address */
#define NVM_MEMORY			((volatile uint16_t *)FLASH_ADDR)

typedef enum flash_cmd_tag {
	ERASE_ROW= 0x02,
	WRITE_PAGE= 0x04,
	ERASE_AUXILARYROW= 0x05,
	WRITE_AUXILARYPAGE= 0x06,
	LOCK_REGION= 0x40,
	UNLOCK_REGION= 0x41,
	SET_POWERREDUCTIONMODE= 0x42,
	CLEAR_POWERREDUCTIONMODE= 0x43,
	PAGE_BUFFERCLEAR= 0x44,
	SET_SECURITYBIT= 0x45,
	INVALID_ALLCACHE= 0x46
} flash_cmd_t;

static void halExecuteCommand(flash_cmd_t cmd)
{
	while (!FLASH_READY());
	NVMCTRL_CTRLA= NVMCTRL_CTRLA_CMDEX(CMD_KEY)| NVMCTRL_CTRLA_CMD(cmd);
	while (!FLASH_READY());
}

int CS_ReadEeprom(CS_EepromParams_t *params)
{
	uint16_t i, *ptr= (uint16_t *)params->data;
	uint32_t addr;

	if (NULL== params)
		return -1;
	if ((uint16_t)(params->address + params->length) > EEPROM_DATA_MEMORY_SIZE)
		return -1;

	addr= (FLASH_EEPROM_BASE_ADDRESS + params->address) / 2;		// start addr of row to read from flash. 1/2 because using 16bit.
	for(i= 0; i < params->length/2; i++)
	{
		ptr[i]= NVM_MEMORY[addr + i];		// read from flash using 16bit buffer.
	}

	return 0;
}

int CS_WriteEeprom(CS_EepromParams_t *params)
{
	uint16_t i, *ptr= (uint16_t *)params->data;
	uint32_t addr;

	if (NULL== params)
		return -1;
	if (params->length > ROW_SIZE)
		return -1;
	if ((uint16_t)(params->address + params->length) > EEPROM_DATA_MEMORY_SIZE)
		return -1;

	addr= ((params->address + FLASH_EEPROM_BASE_ADDRESS) - (params->address + FLASH_EEPROM_BASE_ADDRESS) % ROW_SIZE) / 2;		// start addr of row to write on flash. 1/2 because using 16bit.

	NVM_MEMORY[addr]= 0xffff;		// write on start of row to get addr of row in addr reg.
	halExecuteCommand(ERASE_ROW);	// erase data before write in flash.

	for(i= 0; i < params->length/2; i++)
	{
		DBG_Print("D[%d]:%04X\n",i,ptr[i]);

		NVM_MEMORY[addr + i]= ptr[i];		// write on flash page buffer.
	}

	halExecuteCommand(WRITE_PAGE);			// write cmd to save data from flash page buffer to flash.

	return 0;
}
